﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class GenerateObstacles : MonoBehaviour
    {
        [NonSerialized]
        public List<Obstacle> obstacles = new List<Obstacle>();
        [SerializeField]
        public List<GameObject>obstacleOrigins;

        private void Start()
        {
            var lastObstacleZ = Constants.obstStartFrom;
            for (int i = 0; i < Constants.obstaclesCount; i++)
            {
                float coinZ = UnityEngine.Random.Range(lastObstacleZ + Constants.minObstDistance, lastObstacleZ + Constants.maxObstDistance);
                lastObstacleZ = coinZ;

                var newObstacle = new Obstacle();
                var newObstacleObject = Instantiate(obstacleOrigins[UnityEngine.Random.Range(0,obstacleOrigins.Count)], transform);
                newObstacle.transform = newObstacleObject.transform;
                newObstacle.trackNumber = (byte)UnityEngine.Random.Range(0, 3);
                newObstacle.transform.position = new Vector3(AnimationSwitch.tracks[newObstacle.trackNumber].xPosition, Constants.obstHeight, coinZ);
                obstacles.Add(newObstacle);
            }
        }
    }
}
