﻿using Assets.Scripts.Misc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class AnimationSwitch : MonoBehaviour
    {
        [NonSerialized]
        public byte currentTrack = 1;

        public static Track[] tracks = new Track[]
        {
            new Track{xPosition = -2f },
            new Track{xPosition = 0f },
            new Track{xPosition = 2f }
        };

        private BodyController _controller;
        private Animator _anim;
        private float _speed;
        private float _toSpeed;
        private float _lastStepTime;
        private float _maximumSpeed;
        private float _minimumSpeed;               

        private void Start()
        {
            _controller = GetComponent<BodyController>();
            _anim = GetComponent<Animator>();

            _controller.LeftHandUpped += OnLeftHandUp;
            _controller.LeftHandDowned += OnLeftHandDown;
            _controller.RightHandUpped += OnRightHandUp;
            _controller.RightHandDowned += OnRightHandDown;
            _controller.LeftHandSided += OnLeftHandSide;
            _controller.LeftHandUnSided += OnLeftHandUnSide;
            _controller.RightHandSided += OnRightHandSide;
            _controller.RightHandUnSided += OnRightHandUnSide;
            _controller.Step += OnStep;
        }

        private void Update()
        {
            if (Time.time - _lastStepTime > Constants.stepSpeedDown)
            {
                _toSpeed -= Constants.speedDecrease * Time.deltaTime;
            }
            if (_toSpeed < 0) _toSpeed = 0;
            _speed = Mathf.Lerp(_speed, _toSpeed, Time.deltaTime);
            transform.Translate(Vector3.forward * Time.deltaTime * _speed);
            float animSpeed = (_speed - Constants.walkspeed) / (Constants.runspeed - Constants.walkspeed);
            animSpeed = Mathf.Clamp(animSpeed, 0, 1);
            _anim.SetFloat("SpeedPercent", animSpeed);


            if (_speed < _minimumSpeed) _minimumSpeed = _speed;
            if (_speed > _maximumSpeed) _maximumSpeed = _speed;

            //передвижение по горизонтали
            var posX = Mathf.Lerp(transform.position.x, tracks[currentTrack].xPosition, Time.deltaTime * Constants.horizontalSpeed);
            transform.position = new Vector3(posX, transform.position.y, transform.position.z);
        }

        private void OnStep(object sender, EventArgs e)
        {
            if (_lastStepTime > 0)
            {
                _toSpeed = Constants.speedIncrease / (Time.time - _lastStepTime);
            }
            _lastStepTime = Time.time;
        }

        private void OnLeftHandUp(object sender, EventArgs e)
        {
            _anim.SetTrigger("LeftHandUp");
            Debug.Log("Левая рука поднята");
        }

        private void OnLeftHandDown(object sender, EventArgs e)
        {
            _anim.ResetTrigger("LeftHandUp");
            Debug.Log("Левая рука опущена");
        }

        private void OnRightHandUp(object sender, EventArgs e)
        {
            Debug.Log("Правая рука");
            _anim.SetTrigger("RightHandUp");
        }
        private void OnRightHandDown(object sender, EventArgs e)
        {
            Debug.Log("Правая рука опущена");
            _anim.ResetTrigger("RightHandUp");
        }

        private void OnLeftHandSide(object sender, EventArgs e)
        {
            _anim.SetTrigger("LeftHandSide");
            if (currentTrack > 0) currentTrack--;
        }

        private void OnLeftHandUnSide(object sender, EventArgs e)
        {
            _anim.ResetTrigger("LeftHandSide");
        }

        private void OnRightHandSide(object sender, EventArgs e)
        {
            _anim.SetTrigger("RightHandSide");
            if (currentTrack < tracks.Length-1) currentTrack++;
        }
        private void OnRightHandUnSide(object sender, EventArgs e)
        {
            _anim.ResetTrigger("RightHandSide");
        }
    }
}
