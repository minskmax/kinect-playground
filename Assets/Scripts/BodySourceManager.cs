﻿using Assets.Scripts.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Windows.Kinect;

namespace Assets.Scripts
{
    public class BodySourceManager : IBodySource
    {
        private KinectSensor _sensor;
        private BodyFrameReader _reader;
        private Body[] _data = null;

        public bool isKinectPluggedIn; //Кинект подключен
        public Body[] GetBodyData()
        {
            return _data;
        }

        public void Start()
        {
            _sensor = KinectSensor.GetDefault();

            if (_sensor != null)
            {
                _reader = _sensor.BodyFrameSource.OpenReader();

                if (!_sensor.IsOpen)
                {
                    _sensor.Open();
                }
                isKinectPluggedIn = true;
            }
            else
            {
                Debug.Log("Can`t find kinect sensor!!");
            }
        }

        public void Update()
        {
            if (_reader != null)
            {
                var frame = _reader.AcquireLatestFrame(); //Получаем кадр с положением костей
                if (frame != null)
                {
                    if (_data == null)
                    {
                        _data = new Body[_sensor.BodyFrameSource.BodyCount];
                    }

                    frame.GetAndRefreshBodyData(_data);

                    frame.Dispose();
                    frame = null;
                }
            }
        }

        public void Disconnect() //Отключаемся от сенсора при выходе
        {
            if (_reader != null)
            {
                _reader.Dispose();
                _reader = null;
            }

            if (_sensor != null)
            {
                if (_sensor.IsOpen)
                {
                    _sensor.Close();
                }

                _sensor = null;
            }
        }
    }
}
