﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Windows.Kinect;

namespace Assets.Scripts.Interfaces
{
    public interface IBodySource
    {
        Body[] GetBodyData();
        /// <summary>
        /// Инициализация сенсора
        /// </summary>
        void Start();
        /// <summary>
        /// Обновление данных сенсора
        /// </summary>
        void Update();
        /// <summary>
        /// Отключение сенсора при выходе
        /// </summary>
        void Disconnect();
    }
}
