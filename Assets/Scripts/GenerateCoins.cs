﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class GenerateCoins:MonoBehaviour
    {
        [NonSerialized]
        public List<Coin> coins = new List<Coin>();
        public GameObject coinOriginal;

        private void Start()
        {
            var lastCoinZ = Constants.coinsStartFrom;
            for (int i = 0; i < Constants.coinsCount; i++)
            {
                float coinZ = UnityEngine.Random.Range(lastCoinZ+Constants.minCoinsDistance,lastCoinZ+Constants.maxCoinsDistance);
                lastCoinZ = coinZ;

                var newCoin = new Coin();
                var newCoinObject = Instantiate(coinOriginal, transform);
                newCoin.transform = newCoinObject.transform;
                newCoin.trackNumber = (byte) UnityEngine.Random.Range(0, 3);
                newCoin.transform.position = new Vector3(AnimationSwitch.tracks[newCoin.trackNumber].xPosition, Constants.coinsHeight, coinZ);
                coins.Add(newCoin);
            }
        }
    }
}
