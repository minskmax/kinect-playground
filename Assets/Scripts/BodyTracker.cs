﻿using Assets.Scripts.Extensions;
using Assets.Scripts.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Kinect = Windows.Kinect;

namespace Assets.Scripts
{
    public class BodyTracker:MonoBehaviour //Треке
    {
        public Transform head;
        public Transform leftHand;
        public Transform rightHand;
        public Transform leftFoot;
        public Transform rightFoot;

        private IBodySource _bodySource; //Kinect источник трекинга тела    

        private void Start()
        {
            _bodySource = new BodySourceManager();//todo в дальнейшем избавиться от new в пользу инъекции зависимости
            _bodySource.Start();
        }

        private void Update()
        {
            _bodySource.Update();
            Kinect.Body[] data = _bodySource.GetBodyData();
            if (data != null)//Если получили данные
            {
                foreach (var item in data)
                {
                    if (item.IsTracked)
                    {
                        var headPosition = item.Joints[Kinect.JointType.Head].Position;
                        var leftHandPosition = item.Joints[Kinect.JointType.HandLeft].Position;
                        var rightHandPosition = item.Joints[Kinect.JointType.HandRight].Position;
                        var leftFootPosition = item.Joints[Kinect.JointType.FootLeft].Position;
                        var rightFootPosition = item.Joints[Kinect.JointType.FootRight].Position;

                        head.position = KinectHelper.CamToSpace(headPosition);                        
                        leftHand.position = KinectHelper.CamToSpace(leftHandPosition);
                        rightHand.position = KinectHelper.CamToSpace(rightHandPosition);
                        leftFoot.position = KinectHelper.CamToSpace(leftFootPosition);
                        rightFoot.position = KinectHelper.CamToSpace(rightFootPosition);
                    }
                }
            }
        }

        private void OnApplicationQuit()
        {
            if (_bodySource != null)
            {
                _bodySource.Disconnect();
            }
        }
    }
}
