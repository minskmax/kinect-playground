﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class CoinCatcher:MonoBehaviour
    {
        private GenerateCoins _coins;
        private AnimationSwitch _anim;
        private BodyController _controller;
        private Animator _animator;
        private AudioSource _audio;

        private void Start()
        {
            _coins = FindObjectOfType<GenerateCoins>();
            _anim = GetComponent<AnimationSwitch>();
            _animator = GetComponent<Animator>();
            _controller = GetComponent<BodyController>();
            _audio = GetComponents<AudioSource>()[0];//todo не надо так делать
        }

        private void Update()
        {
           if (_controller.LeftHandUp || _controller.RightHandUp)
            {
                var coinToCatch = _coins.coins.FirstOrDefault(
                    x => x.trackNumber == _anim.currentTrack
                    && x.transform.position.z > transform.position.z - Constants.coinDistanceToCatch
                    && x.transform.position.z < transform.position.z + Constants.coinDistanceToCatch
                    && !x.catched
                    );
                if (coinToCatch!=null) //Ура, монетка!
                {
                    coinToCatch.catched = true;
                    Debug.Log("ee moneetka");                    
                    _audio.Play();
                    _animator.SetTrigger("Catch");
                    coinToCatch.transform.parent = transform;
                    StartCoroutine(coinToCatch.CatchIt());                    
                }
                else
                {
                    //_animator.ResetTrigger("Catch");
                }

            }
            else
            {
                //_animator.ResetTrigger("Catch");
            }
        }
    }
}
