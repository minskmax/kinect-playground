﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class ObstacleCatcher : MonoBehaviour
    {
        public Rigidbody hip;

        private GenerateObstacles _obstacles;
        private AnimationSwitch _anim;
        private BodyController _controller;
        private Animator _animator;
        private AudioSource _audio;        

        private void Start()
        {
            _obstacles = FindObjectOfType<GenerateObstacles>();
            _anim = GetComponent<AnimationSwitch>();
            _animator = GetComponent<Animator>();
            _controller = GetComponent<BodyController>();
            _audio = GetComponents<AudioSource>()[1];
            
        }

        private void Update()
        {            
                var obstacleToCatch = _obstacles.obstacles.FirstOrDefault(
                    x => x.trackNumber == _anim.currentTrack
                    && x.transform.position.z > transform.position.z
                    && x.transform.position.z < transform.position.z + Constants.obstDistanceToCatch
                    && x.transform.gameObject.activeSelf
                    );
                if (obstacleToCatch != null)
                {
                    Debug.Log("potracheno");
                _audio.Play();                
                _anim.enabled = false;
                hip.isKinematic = false;
                _animator.enabled = false;
                this.enabled = false;
                }                            
        }
    }
}
