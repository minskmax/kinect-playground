﻿using Assets.Scripts.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Kinect = Windows.Kinect;

namespace Assets.Scripts
{
    public class BodyController : MonoBehaviour
    {
        public Kinect.CameraSpacePoint HeadPosition { get; private set; }
        public Kinect.CameraSpacePoint LeftHandPosition { get; private set; }
        public Kinect.CameraSpacePoint RightHandPosition { get; private set; }
        public Kinect.CameraSpacePoint LeftFootPosition { get; private set; }
        public Kinect.CameraSpacePoint RightFootPosition { get; private set; }

        public bool LeftHandUp { get; private set; }
        public bool RightHandUp { get; private set; }
        public bool LeftHandSide { get; private set; }
        public bool RightHandSide { get; private set; }

        public bool LeftFootUp { get; private set; }
        public bool RightFootUp { get; private set; }

        /// <summary>
        /// Событие вызываемое при совершенном шаге
        /// </summary>
        public event EventHandler<EventArgs> Step;
        /// <summary>
        /// При поднятии руки
        /// </summary>
        public event EventHandler<EventArgs> LeftHandUpped;
        public event EventHandler<EventArgs> LeftHandDowned;
        public event EventHandler<EventArgs> RightHandUpped;
        public event EventHandler<EventArgs> RightHandDowned;
        /// <summary>
        /// При поднятии двух рук
        /// </summary>
        public event EventHandler<EventArgs> BothHandUpped;

        public event EventHandler<EventArgs> LeftHandSided;
        public event EventHandler<EventArgs> LeftHandUnSided;
        public event EventHandler<EventArgs> RightHandSided;
        public event EventHandler<EventArgs> RightHandUnSided;
        /// <summary>
        /// Начало трекинга положения тела
        /// </summary>
        public event EventHandler<EventArgs> TrackingStart;
        /// <summary>
        /// Потеря трэкинга
        /// </summary>
        public event EventHandler<EventArgs> TrackingLost;
        /// <summary>
        /// Рука вытянута в сторону
        /// </summary>
        public event EventHandler<EventArgs> HandSide;

        private IBodySource _bodySource;
        private bool _isTracked;
        private double _lastLeftFootUpTime;
        private double _lastRightFootUpTime;        

        private void Start()
        {
            _bodySource = new BodySourceManager();//todo в дальнейшем избавиться от new в пользу инъекции зависимости
            _bodySource.Start();
        }

        private void Update()
        {
            if (_bodySource == null) return;
            _bodySource.Update();
            Kinect.Body[] data = _bodySource.GetBodyData();
            if (data != null)//Если получили данные
            {
                if (!_isTracked)//Начало трекинга
                {
                    if (TrackingStart != null) TrackingStart(this, EventArgs.Empty);
                    _isTracked = true;
                }
                var body = data.FirstOrDefault(x => x.IsTracked);
                if (body != null) //Если есть хотяб одно тело для трекинга записываем положения ключевых точек
                {
                    HeadPosition = body.Joints[Kinect.JointType.Head].Position;
                    LeftHandPosition = body.Joints[Kinect.JointType.HandLeft].Position;
                    RightHandPosition = body.Joints[Kinect.JointType.HandRight].Position;
                    LeftFootPosition = body.Joints[Kinect.JointType.FootLeft].Position;
                    RightFootPosition = body.Joints[Kinect.JointType.FootRight].Position;

                    if (IsLeftHandUp())
                    {
                        if (!LeftHandUp)//Если рука только что поднята
                        {
                            LeftHandUp = true;
                            Debug.Log("Левая рука поднята!");
                            if (LeftHandUpped != null) LeftHandUpped(this, EventArgs.Empty);
                            if (RightHandUp)
                            {
                                Debug.Log("Обе руки подняты!");
                                if (BothHandUpped != null) BothHandUpped(this,EventArgs.Empty);
                            }                                                                                        
                        }
                    }
                    else
                    {
                        if (LeftHandUp)
                        {
                            if (LeftHandDowned != null) LeftHandDowned(this, EventArgs.Empty);
                            LeftHandUp = false;
                        }
                    }

                    if (IsRightHandUp())
                    {
                        if (!RightHandUp)//Если рука только что поднята
                        {
                            RightHandUp = true;
                            Debug.Log("Правая рука поднята!");
                            if (RightHandUpped != null) RightHandUpped(this, EventArgs.Empty);
                            if (LeftHandUp)//Проверка две руки подняты или одна, если обе то вызываем соответствующее событие
                            {
                                Debug.Log("Обе руки подняты!");
                                if (BothHandUpped != null) BothHandUpped(this, EventArgs.Empty);
                            }                                                                                        
                        }
                    }
                    else
                    {
                        if (RightHandUp)
                        {
                            if (RightHandDowned != null) RightHandDowned(this, EventArgs.Empty);
                            RightHandUp = false;
                        }
                    }

                    if (IsLeftFootUp())
                    {
                        if (!LeftFootUp)
                        {
                            LeftFootUp = true;
                            Debug.Log("Левая нога поднята!!");                            
                            if (Time.time-_lastRightFootUpTime<Constants.secondsWaitForStep)//Если правая нога была поднята не ранее чем n секунд назад то засчитываем это движение как шаг
                            {
                                if (Step != null) Step(this,EventArgs.Empty);
                                Debug.Log("Шаг сделан!!");
                            }
                            _lastLeftFootUpTime = Time.time;
                        }
                    }
                    else
                    {
                        LeftFootUp = false;
                    }

                    if (IsRightFootUp())
                    {
                        if (!RightFootUp)
                        {
                            RightFootUp = true;
                            Debug.Log("Правая нога поднята!!");
                            if (Time.time - _lastLeftFootUpTime < Constants.secondsWaitForStep)
                            {
                                if (Step != null) Step(this, EventArgs.Empty);
                                Debug.Log("Шаг сделан!!");
                            }
                            _lastRightFootUpTime = Time.time;
                        }
                    }
                    else
                    {
                        RightFootUp = false;
                    }

                    if (IsLeftHandSide())
                    {
                        if (!LeftHandSide)//Рука только что отведена в сторону
                        {
                            LeftHandSide = true;
                            if (LeftHandSided != null) LeftHandSided(this, EventArgs.Empty);
                        }
                    }
                    else
                    {
                        if (LeftHandSide)
                        {
                            if (LeftHandUnSided != null) LeftHandUnSided(this, EventArgs.Empty);
                            LeftHandSide = false;
                        }
                    }

                    if (IsRightHandSide())
                    {
                        if (!RightHandSide)//Рука только что отведена в сторону
                        {
                            RightHandSide = true;
                            if (RightHandSided != null) RightHandSided(this, EventArgs.Empty);
                            Debug.Log("правая в сторону");
                        }
                    }
                    else
                    {
                        if (RightHandSide)
                        {
                            if (RightHandUnSided != null) RightHandUnSided(this, EventArgs.Empty);
                            RightHandSide = false;
                            Debug.Log("правая обратно");
                        }
                    }
                }
            }
            else
            {
                if (_isTracked) //Потеря трекинга
                {
                    if (TrackingStart != null) TrackingLost(this, EventArgs.Empty);
                    _isTracked = false;
                    LeftHandUp = false;
                    RightHandUp = false;
                    LeftFootUp = false;
                    RightFootUp = false;
                }
            }
        }

        private void OnApplicationQuit()
        {
            if (_bodySource == null) return;
            _bodySource.Disconnect(); //todo реализовать паттерн IDisposable у BodySourceManager
        }

        /// <summary>
        /// Проверка на поднятие левой руки
        /// </summary>
        /// <returns></returns>
        private bool IsLeftHandUp()
        {
            if (LeftHandPosition.Y > HeadPosition.Y + Constants.oneHandToHead) //Определяем поднята ли рука сравнивая с Y положением головы
            {
                return true;                
            }            
            return false;
        }

        /// <summary>
        /// Проверка на поднятие правой руки
        /// </summary>
        /// <returns></returns>
        private bool IsRightHandUp()
        {
            if (RightHandPosition.Y > HeadPosition.Y + Constants.oneHandToHead) 
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Проверка поднята ли левая нога
        /// </summary>
        /// <returns></returns>
        private bool IsLeftFootUp()
        {
            if (LeftFootPosition.Y-RightFootPosition.Y>Constants.footStepDifference)
            {
                return true;
            }
            return false;
        }

        private bool IsRightFootUp()
        {
            if (RightFootPosition.Y - LeftFootPosition.Y > Constants.footStepDifference)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Левая рука вытянута в сторону
        /// </summary>
        /// <returns></returns>
        private bool IsLeftHandSide()
        {
            if (LeftHandPosition.X < HeadPosition.X - Constants.sideHandToHead) 
            {
                return true;
            }
            return false;
        }

        private bool IsRightHandSide()
        {
            if (RightHandPosition.X > HeadPosition.X + Constants.sideHandToHead)
            {
                return true;
            }
            return false;
        }
    }    
}
