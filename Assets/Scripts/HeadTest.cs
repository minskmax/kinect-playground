﻿using Assets.Scripts;
using Assets.Scripts.Extensions;
using Assets.Scripts.Interfaces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kinect = Windows.Kinect;

public class HeadTest : MonoBehaviour { //Первый тестовый скрипт, попробуем трекинг головы


    private IBodySource _bodySource; //Kinect источник трекинга тела    

	void Start () {
        _bodySource = new BodySourceManager();//todo в дальнейшем избавиться от new в пользу инъекции зависимости
        _bodySource.Start();        
	}
	
	
	void Update () {
        _bodySource.Update();
        Kinect.Body[] data = _bodySource.GetBodyData();
        if (data!=null)//Если получили данные
        {
            foreach (var item in data)
            {
                if (item.IsTracked)
                {                    
                    var headPosition = item.Joints[Kinect.JointType.Head].Position;                    
                    transform.position = KinectHelper.CamToSpace(headPosition);
                }
            }
        }
    }

    private void OnApplicationQuit()
    {
        _bodySource.Disconnect();
    }
}
