﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class Coin
    {
        public Transform transform;
        public byte trackNumber;
        public bool catched;

        public IEnumerator CatchIt()
        {            
            for (int i = 0; i <60; i++)
            {
                transform.Translate(Vector3.down * Time.deltaTime * Constants.coinFallSpeed);
                yield return null;
            }
            //yield return new WaitForSeconds(1f);           
            transform.gameObject.SetActive(false);
        }        
    }
}
