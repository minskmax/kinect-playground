﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public static class Constants//Статический класс для хранения всяких констант
{
    /// <summary>
    /// Разница между положением ног по Y, чтоб засчитать подъем ноги
    /// </summary>
    public static readonly float footStepDifference = 0.08f;
    /// <summary>
    /// Разница между головой и нижней из рук чтоб засчитать подъем рук
    /// </summary>
    public static readonly float twoHandsToHead = 0.5f;
    /// <summary>
    /// Разница между положением рук по Y чтобы засчитать поднятую руку
    /// </summary>
    public static readonly float oneHandToHead = 0.2f;
    /// <summary>
    /// Расстояние между головой и рукой по Х координате чтоб засчитать жест "рука в сторону"
    /// </summary>
    public static readonly float sideHandToHead = 0.6f;
    /// <summary>
    /// Сколько ждать до поднятия другой ноги чтоб засчитать шаг
    /// </summary>
    public static readonly float secondsWaitForStep = 2f;
    /// <summary>
    /// Ускорение персонажа
    /// </summary>
    public static readonly float speedIncrease = 1f;

    public static readonly float speedDecrease = 10f;

    public static readonly float stepSpeedDown = 1f;

    public static readonly float walkspeed = 0f;
    public static readonly float runspeed = 4f;

    public static readonly float horizontalSpeed = 2f;

    public static readonly int coinsCount = 300;
    public static readonly float maxCoinsDistance = 20f;
    public static readonly float minCoinsDistance = 2f;
    public static readonly float coinsHeight = 2f;
    public static readonly float coinsStartFrom = 10f;
    public static readonly float coinDistanceToCatch = 3f;
    public static readonly float coinFallSpeed = 3f;

    public static readonly int obstaclesCount = 300;
    public static readonly float maxObstDistance = 20f;
    public static readonly float minObstDistance = 2f;
    public static readonly float obstHeight = 0;
    public static readonly float obstStartFrom = 10f;
    public static readonly float obstDistanceToCatch = 1f;    
}

